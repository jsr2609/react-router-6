import { createContext, useState } from "react";

interface DataContext {
    isLogged: boolean;
    signIn?: () => void;
    signOut?: () => void;
}

interface Props {
    children: JSX.Element
}
export const AuthContext = createContext<DataContext>({ isLogged: false });

export const AuthProvider = ({ children }: Props) => {
    const [isLogged, setIsLogged] = useState(false);

    const signIn = () => {
        setIsLogged(true);
    }
    const signOut = () => {
        setIsLogged(false);
    }
    return (
        <AuthContext.Provider value={{
            isLogged,
            signIn,
            signOut
        }}>
            {children}
        </AuthContext.Provider>
    );
}