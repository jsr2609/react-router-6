import React, { ChangeEvent, FormEvent, useContext, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { AuthContext } from '../contexts/AuthContext'

const Login = () => {
  const { signIn } = useContext(AuthContext);
  const [secret, setSecret] = useState<string>("");
  const navigate = useNavigate();
  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    if (secret === "Isaac") {
      if (signIn) signIn();
      navigate("/user");
    }
  }

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setSecret(e.target.value);

  }

  return (
    <>
      <div>Login</div>
      <div>
        <form action="" method='POST' onSubmit={handleSubmit}>
          <input type="text" onChange={handleChange} />
          <button type="submit">Iniciar Sesión</button>
        </form>
      </div>
    </>

  )
}

export default Login