import React, { useContext, MouseEvent } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import { AuthContext } from '../contexts/AuthContext'

const Header = () => {
    const {isLogged, signOut} = useContext(AuthContext);
    const navigate = useNavigate();
    const handleLogout = (e: MouseEvent<HTMLAnchorElement>) => {
        e.preventDefault();
        if(signOut) signOut();
        navigate("/");
    }

    return (
        <header>
            <div style={{display: "flex"}}>
            <span>LOGO</span><h1>Encabezado</h1>
            {
                isLogged ? 
                    <Link to="/" onClick={handleLogout}>Logout</Link> :
                    <Link to="/login" >Login</Link>
            }
            </div>
            
            
        

        </header>

    )
}

export default Header