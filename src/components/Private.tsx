import React, { useContext } from 'react'
import { Navigate, Outlet } from 'react-router-dom'
import { AuthContext } from '../contexts/AuthContext';

const Private = () => {
    const {isLogged} = useContext(AuthContext);
    if(!isLogged) {
        return <Navigate to="/login" />
    }
    return (
        <>
            <div>Private Zone</div>
            <Outlet></Outlet>
        </>

    )
}

export default Private