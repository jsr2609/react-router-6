import React from 'react';
import logo from './logo.svg';
import './App.css';

import { Outlet } from 'react-router-dom';
import { AuthProvider } from './contexts/AuthContext';
import Profile from './components/Profile';
import Header from './components/Header';


function App() {
  return (
    <AuthProvider>
      <>
      <Header/>
      <div>
      <Outlet/>
      </div>
      </>
      

    </AuthProvider>
      
    
    
  );
}

export default App;
